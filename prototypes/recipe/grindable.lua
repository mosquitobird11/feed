data:extend{
    {
        type = "recipe",
        name = "flour",
        energy_required = 1,
        enabled = true,
        ingredients = {
            {type = "item", name = "wheat", amount = 2}
        },
        result = "flour",
        category = "grinding"
    }
}
