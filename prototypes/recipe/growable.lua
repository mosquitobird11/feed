data:extend{
    {
        type = "recipe",
        name = "wheat",
        energy_required = 1,
        enabled = true,
        ingredients = {
            {type = "item", name = "seeds-wheat", amount = 8}
        },
        result = "wheat",
        category = "growing"
    },
    {
        type = "recipe",
        name = "tomato",
        energy_required = 1.5,
        enabled = true,
        ingredients = {
            {type = "item", name = "seeds-tomato", amount = 8}
        },
        result = "tomato",
        category = "growing"
    }
}
