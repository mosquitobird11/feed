data:extend{
    {
        type = "recipe",
        name = "farm-ready",
        energy = 3,
        enabled = true,
        ingredients = {
            {type = "item", name = "wood", amount = 8}
        },
        result = "farm-ready"
    },
    {
        type = "recipe",
        name = "wind-mill",
        energy = 3,
        enabled = true,
        ingredients = {
            {type = "item", name = "wood", amount = 8}
        },
        result = "wind-mill"
    },
    {
        type = "recipe",
        name = "seeds-wheat",
        energy_required = 0.25,
        enabled = true,
        ingredients = {
            {type = "item", name = "stone", amount = 1}
        },
        result = "seeds-wheat"
    },
    {
        type = "recipe",
        name = "seeds-tomato",
        energy_required = 0.25,
        enabled = true,
        ingredients = {
            {type = "item", name = "stone", amount = 1}
        },
        result = "seeds-tomato"
    }
}
