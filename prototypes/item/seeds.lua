data:extend{
    {
        type = "item",
        name = "seeds-wheat",
        icon = "__Feed__/img/seeds-wheat.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-seeds",
        order="z1"
    },
    {
        type = "item",
        name = "seeds-tomato",
        icon = "__Feed__/img/seeds-tomato.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-seeds",
        order="z2"
    }
}
