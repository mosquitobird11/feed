data:extend{
    {
        type = "item",
        name = "farm-ready",
        icon = "__Feed__/img/farm-ready.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-ground",
        place_result = "farm-ready-entity",
        order="zfarmland"
    },
    {
        type = "item",
        name = "wind-mill",
        icon = "__Feed__/img/feed-group.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-machines",
        place_result = "wind-mill-entity",
        order="zwatermill"
    }
}
