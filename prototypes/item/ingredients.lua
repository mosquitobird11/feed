data:extend{
    {
        type = "item",
        name = "flour",
        icon = "__Feed__/img/feed-group.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-ingredients",
        order="z1"
    }
}
