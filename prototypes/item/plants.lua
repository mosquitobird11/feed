data:extend{
    {
        type = "item",
        name = "wheat",
        icon = "__Feed__/img/wheat.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-plants",
        order="z1"
    },
    {
        type = "item",
        name = "tomato",
        icon = "__Feed__/img/tomato.png",
        icon_size = 32,
        stack_size = 16,
        flags = {},
        subgroup = "feed-subgroup-plants",
        order="z2"
    }
}
