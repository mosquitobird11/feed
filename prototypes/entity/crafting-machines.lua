data:extend{
    {
        type = "assembling-machine",
        name = "farm-ready-entity",
        ingredient_count = 1,
        energy_usage = "1KW",
        crafting_speed = 1,
        crafting_categories = {"growing"},
        energy_source = {
            type = "void"
        },
        order="zfarmland",
        icon = "__Feed__/img/farm-ready.png",
        icon_size = 32,
        flags = {
          "placeable-neutral",
          "placeable-player",
          "player-creation"
        },
        animation = {
            layers = {
                {
                    filename = "__Feed__/img/farm-ready.png",
                    frame_count = 1,
                    height = 32,
                    priority = "high",
                    width = 32
                }
            }
        },
        collision_box = {
            {-0.22,-0.22},
            {0.22,0.22}
        },
        selection_box = {
            {-0.32,-0.32},
            {0.32,0.32}
        },
        show_recipe_icon = false,
        minable = {
            mining_time = 3.0,
            mining_particle = "stone-particle",
            result = "farm-ready"
        }
    },
    {
        type = "assembling-machine",
        name = "wind-mill-entity",
        ingredient_count = 1,
        energy_usage = "1KW",
        crafting_speed = 1,
        crafting_categories = {"grinding"},
        energy_source = {
            type = "void"
        },
        order="zwindmill",
        icon = "__Feed__/img/feed-group.png",
        icon_size = 32,
        flags = {
          "placeable-neutral",
          "placeable-player",
          "player-creation"
        },
        animation = {
            layers = {
                {
                    filename = "__Feed__/img/feed-group.png",
                    frame_count = 1,
                    height = 64,
                    priority = "high",
                    width = 64
                }
            }
        },
        collision_box = {
            {-0.44,-0.44},
            {0.44,0.44}
        },
        selection_box = {
            {-0.64,-0.64},
            {0.64,0.64}
        },
        show_recipe_icon = true,
        minable = {
            mining_time = 3.0,
            mining_particle = "stone-particle",
            result = "wind-mill"
        }
    }
}
