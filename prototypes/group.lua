data:extend{
    {
        type = "item-group",
        name = "feed-group",
        icon = "__Feed__/img/feed-group.png",
        icon_size = 64,
        order = "z"
    },
    {
        type = "item-subgroup",
        name = "feed-subgroup-seeds",
        group = "feed-group",
        order = "a"
    },
    {
        type = "item-subgroup",
        name = "feed-subgroup-plants",
        group = "feed-group",
        order = "b"
    },
    {
        type = "item-subgroup",
        name = "feed-subgroup-ground",
        group = "feed-group",
        order = "c"
    },
    {
        type = "item-subgroup",
        name = "feed-subgroup-machines",
        group = "feed-group",
        order = "d"
    },
    {
        type = "item-subgroup",
        name = "feed-subgroup-ingredients",
        group = "feed-group",
        order = "e"
    }
}
